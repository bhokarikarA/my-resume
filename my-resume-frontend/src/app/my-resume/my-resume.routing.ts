import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CertificatesComponent } from "./certificates/certificates.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { EducationComponent } from "./education/education.component";
import { ExperienceComponent } from "./experience/experience.component";
import { MyResumeComponent } from "./my-resume.component";
import { SkillsComponent } from "./skills/skills.component";


const appRoutes: Routes = [
    {
        path: 'myresume', component: MyResumeComponent, children:[
            {
                path: '', redirectTo: 'dashboard', pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'certificates',
                component: CertificatesComponent
            },
            {
                path: 'education',
                component: EducationComponent
            },
            {
                path: 'experience',
                component: ExperienceComponent
            },
            {
                path: 'skills',
                component: SkillsComponent
            }
        ]

    }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MyResumeRoutingModule{ }