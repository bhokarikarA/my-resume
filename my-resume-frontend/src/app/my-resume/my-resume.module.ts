import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SkillsComponent } from './skills/skills.component';
import { EducationComponent } from './education/education.component';
import { ExperienceComponent } from './experience/experience.component';
import { CertificatesComponent } from './certificates/certificates.component';



@NgModule({
  declarations: [
    DashboardComponent,
    SkillsComponent,
    EducationComponent,
    ExperienceComponent,
    CertificatesComponent
  ],
  imports: [
    CommonModule,
  ],
})
export class MyResumeModule { }
