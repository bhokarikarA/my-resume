import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyResumeComponent } from './my-resume/my-resume.component';

const routes: Routes = [
    {path: 'myResume', component: MyResumeComponent},
    {path: '', component: MyResumeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
