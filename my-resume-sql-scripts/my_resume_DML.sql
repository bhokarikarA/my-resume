use my_data;

-- MY SKILLS
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('Java',1.8,6);
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('Spring Framework',5.3,4);
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('Spring Boot',2.2,4);
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('Angular',11,1);
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('HTML',5,1);
insert into my_skills (skill_name, skill_version, skill_experience_years) values ('CSS',3,1);

-- MY CERTIFICATES
insert into my_certificates (certificate_name, certificate_issuer_company, certificate_skill, certificate_received) values ('eKnowvator Award','Cognizant Technology Solutions','BFS - CSLinks Migration','2016-12-16');
insert into my_certificates (certificate_name, certificate_issuer_company, certificate_skill, certificate_received) values ('Insta Award','Infosys Limited','BFS - ESB + PSH','2018-02-14');
insert into my_certificates (certificate_name, certificate_issuer_company, certificate_skill, certificate_received) values ('Insta Award','Infosys Limited','BFS - IBB + Talent Portal','2021-03-21');

-- MY EDUCATION
insert into my_education (edcuation_level, education_university, education_percentage, education_start_year, education_end_year) values ('SSC','Maharashtra State Board',79.53,2006,2007);
insert into my_education (edcuation_level, education_university, education_percentage, education_start_year, education_end_year) values ('HSC','Maharashtra State Board',61.00,2008,2009);
insert into my_education (edcuation_level, education_university, education_percentage, education_start_year, education_end_year) values ('B.Sc.(Computer Science)','University of Pune',72.5,2009,2012);
insert into my_education (edcuation_level, education_university, education_percentage, education_start_year, education_end_year) values ('M.Sc.(Computer Science)','University of Pune',69.5,2012,2014);

-- MY EXPERIENCE
insert into my_experience (company_name, company_staff_id, company_email_id, company_client, company_location_city, company_location_state, job_start_date, job_end_date) 
	values ('Cognizant Technology Solutions',453270,'aditya.bhokarikar@cognizant.com','Credit Suisse', 'Pune', 'Maharashtra', '2014-08-26','2017-08-04');
insert into my_experience (company_name, company_staff_id, company_email_id, company_client, company_location_city, company_location_state, job_start_date, job_end_date) 
	values ('Infosys Limited',768204,'adityasanjeev.b@infosys.com','Allied Irish Bank', 'Pune', 'Maharashtra', '2017-08-21',NULL);
