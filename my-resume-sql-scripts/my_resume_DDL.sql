use my_data;

CREATE TABLE `my_certificates` (
  `certificate_id` int NOT NULL AUTO_INCREMENT,
  `certificate_name` char(20) NOT NULL,
  `certificate_issuer_company` char(45) NOT NULL,
  `certificate_skill` char(45) NOT NULL,
  `certificate_received` date DEFAULT NULL,
  PRIMARY KEY (`certificate_id`)
);

CREATE TABLE `my_education` (
  `education_id` int NOT NULL AUTO_INCREMENT,
  `edcuation_level` char(30) NOT NULL,
  `education_university` char(45) NOT NULL,
  `education_percentage` decimal(5,3) DEFAULT NULL,
  `education_start_year` int DEFAULT NULL,
  `education_end_year` int DEFAULT NULL,
  PRIMARY KEY (`education_id`),
  UNIQUE KEY `edcuation_level` (`edcuation_level`)
);

CREATE TABLE `my_experience` (
  `job_id` int NOT NULL AUTO_INCREMENT,
  `company_name` char(45) NOT NULL,
  `company_staff_id` char(10) NOT NULL,
  `company_email_id` char(45) NOT NULL,
  `company_client` char(45) DEFAULT NULL,
  `company_location_city` char(10) NOT NULL,
  `company_location_state` char(10) NOT NULL,
  `job_start_date` date DEFAULT NULL,
  `job_end_date` date DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `company_staff_id_UNIQUE` (`company_staff_id`),
  UNIQUE KEY `company_email_id_UNIQUE` (`company_email_id`)

);

CREATE TABLE `my_skills` (
  `skill_id` int NOT NULL AUTO_INCREMENT,
  `skill_name` char(20) NOT NULL,
  `skill_version` float(5,3) DEFAULT NULL,
  `skill_experience_years` int DEFAULT NULL,
  PRIMARY KEY (`skill_id`),
  UNIQUE KEY `skill_name` (`skill_name`),
  UNIQUE KEY `skill_id_UNIQUE` (`skill_id`)
);